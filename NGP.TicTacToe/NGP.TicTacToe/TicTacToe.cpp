
#include "TicTacToe.h"

// Methods
void TicTacToe::DisplayBoard()
{
    cout << "\n\tTic Tac Toe\n\n";
    cout << "Player 1 is X and goes first" << endl << endl;

    cout << "     |     |     " << endl;
    cout << "  " << m_board[1] << "  |  " << m_board[2] << "  |  " << m_board[3] << endl;

    cout << "_____|_____|_____" << endl;
    cout << "     |     |     " << endl;

    cout << "  " << m_board[4] << "  |  " << m_board[5] << "  |  " << m_board[6] << endl;

    cout << "_____|_____|_____" << endl;
    cout << "     |     |     " << endl;

    cout << "  " << m_board[7] << "  |  " << m_board[8] << "  |  " << m_board[9] << endl;

    cout << "     |     |     " << endl << endl;
}

bool TicTacToe::IsOver()
{
    if (m_board[1] == m_board[2] && m_board[2] == m_board[3])
    {
        m_winner = m_playerTurn;
        return 1;
    }
    else if (m_board[4] == m_board[5] && m_board[5] == m_board[6])
    {
        m_winner = m_playerTurn;
        return 1;
    }

    else if (m_board[7] == m_board[8] && m_board[8] == m_board[9])
    {
        m_winner = m_playerTurn;
        return 1;
    }

    else if (m_board[1] == m_board[4] && m_board[4] == m_board[7])
    {
        m_winner = m_playerTurn;
        return 1;
    }
    else if (m_board[2] == m_board[5] && m_board[5] == m_board[8])
    {
        m_winner = m_playerTurn;
        return 1;
    }
    else if (m_board[3] == m_board[6] && m_board[6] == m_board[9])
    {
        m_winner = m_playerTurn;
        return 1;
    }
    else if (m_board[1] == m_board[5] && m_board[5] == m_board[9])
    {
        m_winner = m_playerTurn;
        return 1;
    }
    else if (m_board[3] == m_board[5] && m_board[5] == m_board[7])
    {
        m_winner = m_playerTurn;
        return 1;
    }
    else if (m_numTurns == 9)
    {
        m_winner = 'T';
        return 1;
    }
    else
        return 0;
}

char TicTacToe::GetPlayerTurn()
{
    switch (m_playerTurn)
    {
    case 'X':
        return '1';
        break;
    case 'O':
        return '2';
        break;
    }
}

bool TicTacToe::IsValidMove(int position)
{
    if (m_board[position] == 'X' || m_board[position] == 'O')
        return false;
    return true;
}

void TicTacToe::Move(int position)
{
    m_board[position] = m_playerTurn;
    m_numTurns++;

    switch (m_playerTurn)
    {
    case 'X':
        m_playerTurn = 'O';
        break;
    case 'O':
        m_playerTurn = 'X';
        break;
    }
}

void TicTacToe::DisplayResult()
{
    if (m_winner == 'X')
        cout << "Player 2 is the winner!";
    else if (m_winner == 'O')
        cout << "Player 1 is the winner!";
    else if (m_winner == 'T')
        cout << "It is a tie game!";
}
