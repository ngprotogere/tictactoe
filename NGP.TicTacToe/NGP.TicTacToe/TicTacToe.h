#pragma once

#include <iostream>

using namespace std;

class TicTacToe
{
private:

	// Fields
	char m_board[10] = { 'o','1','2','3','4','5','6','7','8','9' };
	int m_numTurns;
	char m_playerTurn;
	char m_winner;

public:

	// Constructor
	TicTacToe()
	{
		m_numTurns = 0;
		m_playerTurn = 'X';
		m_winner = ' ';
	}


	// Methods
	void DisplayBoard();
	bool IsOver();
	char GetPlayerTurn();
	bool IsValidMove(int position);
	void Move(int position);
	void DisplayResult();
};